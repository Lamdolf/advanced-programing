PDB to Graph
v. 1.2.1

Resumen:
PDB to Graph es un programa diseñado con la finalidad de poder descargar y
procesar archivos provenientes de la protein data bank (pdb) y poder procesarlos
generando un gráfico compuesto de nodos que representan la totalidad de los
residuos de aminoácido presentes y los elementos que componen a cada uno. Además,
cada uno de estos residuos esta unidos según la cadena a la que pertenecen,
permitiendo de forma simple un vistazo a la composición de una determinada
proteína.


Funciones:
-Descarga de archivos PDB desde el servidor de rcsb.org.
-Revisión de proteínas ya descargadas.
-Generación de gráficos en formato ".ps" a partir de un archivo ".pdb".


Instrucciones:
-Para iniciar el programa, basta con ejecutar desde el directorio del programa,
por medio de una terminal el comando "bash pdbtograph.sh" sin entregar ningún
parámetro.
-En el menú principal se pueden seleccionar 4 opciones: descargar proteína,
revisar proteínas disponibles, graficar proteína a partir de pdb y salir.
-Si se selecciona la opción de descargar proteína, se deberá ingresar un código
de 4 dígitos que corresponda solo a una proteína, nada mas. No se preocupe por
mayúsculas o minúsculas.
-Si se selecciona visualizar proteínas disponibles, se listaran las proteínas
que ya se encuentran descargadas (si es que hay al menos una), se dará todo el
tiempo que necesite para revisarlas. Para terminar revisión se debe presionar
la tecla enter.
-Si se selecciona la opción de graficar, se llevara al usuario a un menú en el
que aparecerán todas las proteínas disponibles (siempre y cuando haya almenos
una proteína descargada).
-En el menú de graficar, se puede seleccionar una proteína por el numero que se
le asignará a cada una o ingresar la letra "q" para volver al menú principal.
-A continuación de escoger una se procederá a generar el gráfico siempre y
cuando este no este ya graficado, terminado el proceso se volverá al menú  
principal.
-La última opción corresponde a salir del programa, se dará un mensaje de
despedida y se cerrará el programa.


Algunas consideraciones:
-El programa requiere una base de datos que se descarga automáticamente si esta
no se encuentra en el directorio del programa, sin embargo, de no existir esta
y ademas no tener conexión a internet, el programa no podrá iniciarse.
-De igual manera, no se podrá descargar ninguna proteína si no se puede conectar
con el servidor de rcsb.org, sea por falta de conexión o problemas con la página.
-No se debe ingresar parámetros a la hora de iniciar el programa, de lo contrario
el programa arrojará error (de hecho informará sobre el error y no se iniciará).
-El tiempo de renderizado de cada gráfico dependerá de la capacidad de cada
ordenador (a pesar de que se trabajó en optimizar tiempos).
-Junto al punto anterior, se recomienda probar funcionalidades con proteínas
pequeñas a medianas antes de intentar renderizar cualquier proteína de mayor
tamaño, no solo por el tiempo de renderizado, sino que también por la dificultad
de algunos ordenadores para abrir el archivo generado. Algunas sugerencias para
empezar: 5ZCK, 1ZNI, 1A3N.
-Si por el contrario desea fundir su ordenador, pruebe con la proteína 1IWA...


Créditos:
Autor: Arturo Adolfo Lobos Castillo
Módulo: Programación Avanzada
Profesor de cátedra y laboratorio: Fabio Esteban Duran Verdugo
Fecha: 28 Abril 2019
Carrera: Ingeniería Civil en Bioinformática


Historial de versiones:

Versión 1.0

-Se sube primera versión terminada del código.
-Se comprueba que exista base de datos y se descarga en caso de no existir
esta.
-En caso de no estarlo, la base de datos se procesa para obtener un archivo mas
liviano que solo cuenta con código y tipo de macromolécula, este queda
almacenado en la carpeta "processed_database".
-Comprueba que la proteína seleccionada este en la base de datos y que sea una
proteína, y la descarga, almacenándola en la carpeta "proteínas".
-Se puede revisar en el menú principal las proteínas disponibles.
-Se puede generar un gráfico a partir de un archivo ".pdb".
-Los gráficos se generan en formato ".png" y quedan almacenados en carpeta
"gráficos".


Versión 1.1

-Se cambia formato de salida de archivo ".png" a ".ps" debido a la dificultad de
algunos ordenadores para abrir el archivo.
-Se agrega una condición que chequea que no se entreguen parámetros al abrir el
código.
-Se cambio el diseño de los gráficos, ahora los residuos cuentan con la forma
de "triple octágono" y los elementos de este cuentan con la forma de "casa
invertida".
-Se soluciona un error que se ocasionaba cuando por razón desconocida, un
archivo ".pdb" tenia 4 letras en vez de 3 en la columna de residuo.
-Ahora ya no se puede descargar y reemplazar una proteína que ya esta guardada
en la carpeta proteínas.
-La totalidad de los diálogos ahora están en español.
-Ahora se corta correctamente el nombre del archivo ".ps" generado (antes eran
cosas como "1A3N.pdb.ps", ahora solo es "1A3N.ps")

Versión 1.2

-Ahora se comprueba que se tenga conexión a internet a la hora de querer
descargar la base de datos o alguna proteína, no pudiendo iniciar el programa
si no se encuentra la base de datos y ademas no hay internet.
-Ya no se puede graficar una proteína si ya existe un grafico generado para
esta proteína, se da un aviso de que este ya existe.
-Se cambia extensión del programa final de ".bash" a ".sh" por temas de
elegancia (nótese la subjetividad).
-Algunas partes del programa fueron cortadas debido a redundancia del código,
acortando este en unas cuantas lineas y conservando su funcionalidad.
-Ya no se podrá listar las proteínas disponibles si la carpeta de proteínas
se encuentra vacía (antes solo se comprobaba si esta existía, ahora ademas se
comprueba que no este vacía).
-De igual manera ya no se puede acceder al modulo de creación de gráficos si el
directorio de proteínas esta vacío.
-Corregido un error que hacia que si se presionaba enter antes de ingresar el
código de la proteína, el programa quedada sin hacer nada y se debía salir con
ctrl + c.
-Corregido un error que hacia que cuando se volvía al menú principal después de
haber usado el modulo de creación de gráficos y posteriormente se seleccionaba
la opción de salir, el programa volvía al modulo de creación de gráficos.
-Ahora se chequea que ademas de existir la carpeta de la base de datos
procesada, ademas se encuentre el archivo en cuestión (antes solo se revisaba
si existía la carpeta).
-Ahora la totalidad del código (incluyendo los archivos ".awk"), se encuentra
comentado para su mayor comprensión.

Versión 1.2.1

-Se corrigen faltas de ortografía en algunas secciones del código.
-Se optimiza ligeramente el tiempo de renderizado de imágenes a partir de
archivo ".dot" (por ejemplo 1ZNI demoraba 3 seg, ahora demora 2. 1A3N
demoraba 1 min 20 seg, ahora demora aprox 40 seg) en desmedro una calidad mas
pequeña (casi imperceptible por la escala de los gráficos).
-Se quita mas código redundante.
-Se agrega archivo readme.
