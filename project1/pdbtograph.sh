#!/bin/bash


#Función para descargar la proteína en caso de ser válida
function downloader {

    #En caso de no existir la carpeta para almacenar las proteínas
    #se crea una con el nombre "proteínas"
    if [[ ! -d "proteínas" ]]; then

        mkdir proteínas
        cd proteínas
    fi

    #Se cambia de directorio y se usa el comando wget para descargar
    #la proteína seleccionada desde rcsb.org, solo si se puede conectar
    #con el servidor
    cd proteínas

    wget -q --spider https://files.rcsb.org
    if [[ $? ]]; then
        echo "Descargando la proteína $1"
        wget https://files.rcsb.org/download/$1.pdb
        cd ..
        #Se devuelve al directorio principal
        echo "Listo! (presione enter para continuar)"
        read aux

    else
        echo "No se puede conectar al servidor de descargas"
        echo "Por favor, revise su conexión a internet"
    fi
}


#Función para revisar si el código ingresado es válido
function type_check {

    #Primero se comprueba si la el código esta en la base
    valid="1"
    match="$(grep $1 processed_database/codes.txt)"

    if [[ $match != "" ]]; then
        type="$(echo $match | awk '{print $3}')"

        #En caso de estar, se revisa si es una proteína o si es que
        #ya está descargada
        if [[ -f proteínas/$1.pdb ]]; then
            echo "La proteína seleccionada ya se encuentra descargada"

        elif [[ ${type:1:-1} == "Protein" ]]; then
            valid="0"

        else
            echo "El código ingresado no corresponde a una proteína"
        fi

    else
        echo "No se puede encontrar el código ingresado en la base de datos"
    fi

    #Sea cual sea el caso, se retorna un boleano
    return $valid
}


#Función que revisa base de datos
function database_checker {

    echo "Chequeando base de datos"
    sleep 1

    #Primero se revisa si existe la base de datos
    if [[ ! -f "bd-pdb.txt" ]]; then

        #Despues se chequea que exista conexión con la página para
        #descargar la base de datos
        wget -q --spider https://icb.utalca.cl/~fduran/bd-pdb.txt
        if [[ $? ]]; then
            echo "Descargando base de datos..."

            #En caso de estarlo, se descarga desde icb.utalca.cl
            wget https://icb.utalca.cl/~fduran/bd-pdb.txt
            echo "Listo!"

        else
            echo "No hay conexión disponible para descargar base de datos"
            echo "Por favor, conéctese a internet y vuelva a intentarlo"
            exit 1
        fi
    fi

    #Luego se revisa si la base esta procesada
    #En caso de no estarlo, se crea una carpeta y procesa la base de
    #datos, reduciendo el tamaño de la base original y quedando
    #solo con la información necesaria (código y tipo)

    if [[ ! -d "processed_database" ]]; then
        $(mkdir processed_database)
    fi

    if [[ ! -f "processed_database/codes.txt" ]]; then
        echo "Procesando base de datos"
        awk -F ',' '{print $1 " - " $NF}' bd-pdb.txt > processed_database/codes.txt
        echo "Listo!"
        sleep 1

    fi
    clear
}


#Función de titulo (principal)
function title {

    #Impresión de pantalla de título
    echo "+----------------------------------------------+"
    echo "|               PDB TO GRAPHIC                 |"
    echo "|                   v1.2.1                     |"
    echo "+----------------------------------------------+"
    echo ""
    echo "¿Qué desea hacer?"
    echo "1.- Descargar proteína"
    echo "2.- Revisar proteínas disponibles"
    echo "3.- Generar gráfico a partir de una proteína"
    echo "4.- Encontrar ligandos en una proteína"
    echo "5.- Salir"
    echo ""

    read option

    #Si se elige opción uno, se pregunta código de proteína
    if [[ $option == "1" ]]; then
        echo "Por favor ingrese el código de su proteína"
        echo "(no distingue mayúsculas o minúsculas)"
        read protein_code

        #Se transforman todos los caracteres a mayúsculas y se envía a
        #función para chequear validez
        if [[ $(expr length "$protein_code") != "4" ]]; then
            #Hasta ahora, todos los códigos son de 4 letras
            echo "Código no válido"
            sleep 1

        elif (type_check ${protein_code^^}); then
            downloader ${protein_code^^}

        else
            sleep 1
        fi

    #Si se elige opción 2, se listan todos los archivos contenidos en la
    #carpeta "proteínas", si y solo si existe la carpeta y no esta vacía
    elif [[ $option == "2" ]]; then
        if [[ -d "proteínas" && $(ls proteínas/ | wc -l) != 0 ]]; then
            echo "Las proteínas disponibles son: "
            ls proteínas/
            echo "(Presione enter para continuar)"
            read aux

        else
            echo "No hay proteínas disponibles"
            sleep 1
        fi

    #Si se elige opción 3, se llama a módulo para creacción de gráficos
    #solo si existe almenos una proteína disponible
    elif [[ $option == "3" ]]; then
        if [[ ! -d "proteínas" || $(ls proteínas/ | wc -l) == "0" ]]; then
            echo "No existen proteínas disponibles para graficar"
            sleep 2

        else
            bash graphic_creator.sh
            exit 0 #Exit para evitar que se abran dos procesos paralelos
        fi

    elif [[ $option == "4" ]]; then
        if [[ ! -d "proteínas" || $(ls proteínas/ | wc -l) == "0" ]]; then
            echo "No existen proteínas disponibles para buscar ligandos"
            sleep 2

        else
            bash graphic_creator.sh -l
            exit 0 #Exit para evitar que se abran dos procesos paralelos
        fi

    #Se da un mensaje de despedida <3 y sale con código 0
    elif [[ $option == "5" ]]; then
        echo "Adiós <3"
        sleep 1
        exit 0

    #Cualquier otra opción no sera válida, se informará de esto y se
    #limpiará la pantalla quedando solo el menu de título
    else
        echo "Opción no válida"
        sleep 1
    fi

    clear
    title
}


#El programa no recibe parametros, en caso de hacerlo arroja error y se
#cierra con codigo 1. Si no se pasan parametros, recien se llama a
#funciones principales
if [[ $1 != "" ]]; then
    echo "Error, no se deben ingresar parámetros. Use bash pdbtograph.sh"
    exit 1

else
    database_checker
    title
fi
