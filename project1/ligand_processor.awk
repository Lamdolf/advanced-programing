BEGIN{
    aux = 0
    n = 0
}

{
    if (length($4) > 3){
        ligand_aux = substr($4,2,3)
    }
    else{
        ligand_aux = $4
    }


    if ($1 == "HETATM" && ligand_aux != "HOH"){

        actual = $5$6

        if(actual != anterior && aux != 0){

            print "CENTER", ligando, anterior, cor_x/n, cor_y/n, cor_z/n
            cor_x = 0
            cor_y = 0
            cor_z = 0
            n = 0
        }

        print $3, $4, $5, $6
        cor_x += $7
        cor_y += $8
        cor_z += $9
        ligando = $4
        anterior = actual
        aux = 1
        n++
    }
}

END{
    print "CENTER", ligando, anterior, cor_x/n, cor_y/n, cor_z/n
}
