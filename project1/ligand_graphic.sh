#!/bin/bash


function menu {

    echo "¿A que distancia en angstrom quiere comprobar ligandos?"
    read distance

    if [[ ! "$distance" =~ ^[0-9]+$ || "$distance" -lt 1 ]]; then
        echo "El valor entregado no es válido"
        sleep 1
        clear
        menu
    fi

    echo "Las proteínas disponibles son las siguientes: "
    ls proteínas/ | awk '{ printf "%s", NR " " $1; $1=""; print $0 }'>temp
    cat temp

    max="$(cat temp | wc -l)"
    echo "Por favor, seleccione una proteína por su número"
    echo "(o ingrese q para volver al menu principal)"
    read protein

    if [[ "$protein" =~ ^[0-9]+$ ]]; then
        if [[ "$protein" -gt "0" && "$protein" -le "$max" ]]; then

            selected="$(sed "$protein q;d" temp)"
            rm temp

            if [[ -f "ligandos/${selected:2:-4}$distance" ]]; then
                echo "Ya se ha graficado una proteína con estos parametros"

            else
                generate ${selected:2}

            fi

        else
            echo "opcion"




}


if [[ ! -f "ligandos" ]]; then
    mkdir ligandos
fi

menu
