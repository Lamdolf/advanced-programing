#!/bin/bash


function ligand_generator {

    #Se desplaza a directorio de proteínas por comodidad
    cd proteinas
    echo "Comprobando distancias..."

    #Primero se procesan los átomos de la proteína
    #luego se procesan los ligandos de la proteína
    #finalmente con los dos archivos obtenidos (contienen nombres y
    #coordenadas espaciales), se comprueba la distancia de cada centro
    #geométrico con cada átomo (ver archivos awk para detalles)
    awk -f ../procesor.awk $1 > $1.temp
    awk -f ../ligand_processor.awk $1 > ligand$1.temp
    awk -v limit=$distance -f ../distances.awk ligand$1.temp $1.temp >> ligand$1.temp

    #Con los datos obtenidos, se crea un archivo.dot
    awk -f ../ligand_creator.awk ligand$1.temp $1.temp > $1.dot

    #Se borran los archivos temporales previamente creados
    rm $1.temp
    rm ligand$1.temp

    #Se renderiza gráfico a partir de archivo .dot, ademas se entrega
    #el tiempo que este demoró en completarse. Luego se borra el archivo
    #.dot y se vuelve al directorio principal
    time dot -Tps -o ../ligandos/${1::-4}_$distance.ps $1.dot
    rm $1.dot
    cd ..

    #Mensajes para el usuario
    echo "Listo!"
    echo "El gráfico se encuentra en la carpeta llamada ligandos"
    echo "(presione enter para volver al menu principal)"
    read aux
    bash pdbtograph.sh
    exit 0

}


function protein_generator {

    #Por comodidad, se trasladará momentaneamente al directorio donde
    #se almacenan las proteínas
    cd proteinas
    echo "Generando gráfico..."

    #Primero se procesará el archivo pdb a uno mas simple (temporal)
    #para facilitar su manipulación
    awk -f ../procesor.awk $1 > $1.temp
    awk -f ../graph_creator.awk $1.temp > $1.dot #detalles en archivo .awk
    rm $1.temp

    #En caso de no existir carpeta para guardar gráficos, se crea una
    if [[ ! -d "../graficos" ]]; then
        mkdir ../graficos
    fi

    #Se corre graphviz para generar gráfico a partir de archivo .dot
    #recién creado, posterior a esto, se mueve el gráfico a la carpeta
    #contenedora y se borra el archivo .dot
    time dot -Tps -o ${1::-4}.ps $1.dot
    mv ${1::-4}.ps ../graficos
    rm $1.dot #comentar esta línea si se quiere revisar archivo .dot
    cd ..

    #Se da un mensaje avisando de la ubicación del gráfico y se retorna
    #al menu principal
    echo "Listo!"
    echo "El gráfico se encuentra en la carpeta llamada graficos"
    echo "(presione enter para volver al menu principal)"
    read aux
    bash pdbtograph.sh
    exit 0
}


#Funcion principal de modulo creador, basicamente un menu de opciones
function generator_menu {

    #Se listan las proteínas y se crea un archivo temporal para ayudar
    #a una selección de proteínas mas cómoda
    echo "Las proteínas disponibles son las siguientes: "
    ls proteinas/ | awk '{ printf "%s", NR " " $1; $1=""; print $0 }'>temp
    cat temp

    max="$(cat temp | wc -l)"
    echo "Por favor, seleccione una proteína por su número"
    echo "(o ingrese q para volver al menu principal)"
    read protein

    #Se chequea que solo se ingresen números y que ademas sea un numero
    #válido (depende de la cantidad de proteínas)
    if [[ "$protein" =~ ^[0-9]+$ ]]; then
        if [[ "$protein" -gt "0" && "$protein" -le "$max" ]]; then

            #Se selecciona solo 1 elemento del archivo temp y se borra
            selected="$(sed "$protein q;d" temp)"
            rm temp

            #Se comprueba si se entregó algún parámetro
            if [[ $1 == "0" ]]; then

                #En caso de haberse enviado, llama a función que busca
                #ligandos en la proteína, si es que no se ha graficado
                #ya esa proteína con la misma distancia de busqueda
                if [[ -f "ligandos/${selected:2:-4}$distance.ps" ]]; then
                    echo "Ya se ha graficado esta proteína con esta distancia"

                else
                    ligand_generator ${selected:2}

                fi


            #Si no se envió parámetro, se procede con función para graficar
            #proteína completa, solo si esta no esta graficada ya
            else

                if [[ -f "graficos/${selected:2:-4}.ps" ]]; then
                    echo "La proteína seleccionada ya se encuentra graficada"

                else
                    protein_generator ${selected:2}
                fi
            fi

        else
            echo "Opción no válida"
        fi

    #Además se cuenta con una opción para volver al menu principal si se
    #ingresa la letra "q" en vez de un número
    elif [[ "$protein" == "q" ]]; then
        clear
        bash pdbtograph.sh
        rm temp
        exit 0

    else
        echo "Opción no válida"
    fi

    #Ante cualquier error, se informara y se limpia la pantalla para
    #volver a ingresar una opción
    sleep 1
    clear
    generator_menu
}


ligand="1" #Se declara un boleano en false

if [[ $1 == "-l" ]]; then

    #En caso de entregarse un parámetro, este boleano pasa a ser verdadero
    #y se pregunta a que distancia en angstrom se desea comprobar la
    #presencia de ligandos
    ligand="0"
    echo "¿A que distancia desea comprobar la existencia de ligandos?"
    echo "(medido en angstrom, para decimales, utilice un punto y no una coma)"
    read distance

    #En caso de no existir directorio para guardar ligandos, se crea uno
    if [[ ! -d "ligandos" ]]; then
        mkdir ligandos
    fi
fi

generator_menu $ligand
