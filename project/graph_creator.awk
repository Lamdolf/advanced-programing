BEGIN{

    #Se crea variable auxiliar para poder imprimir " y se inicia gráfico
    x="\""
    print "digraph G {"
    print "nslimit=1"
    print "nslimit1=1"
    print "maxiter=3"
    print "mclimit=0.1"
    print "searchsize=5"
}

{
    actual=$2$3$4

    #Se revisa si el residuo actual es igual al anterior
    if(actual != anterior){

        #En caso de no serlo, se crea un nuevo modulo con el nombre
        #del residuo
        print ""
        print "node [shape=tripleoctagon]"
        print actual " [label = "x$2x"]"

        #Solo si son de la misma cadena, estos se uniran entre si
        if ($3 == cadena_anterior){
            print actual " -> " anterior "[label = cadena"$3"]"
        }

        print ""
        print "node [shape=invhouse]"
    }

    #Se crea un nuevo nodo por cada elemento, para que no se repitan
    #se toma el nombre del elemento, la cadena y el número de residuo
    print $1$3$4 " [label = "x$1x"]"
    print $1$3$4 " -> " actual "[arrowhead = vee]"

    #Se guardan datos en dos variables al final del bucle, esto ayuda
    #a la conexión y comparación de lineas
    anterior=actual
    cadena_anterior=$3
}

END{
    #Se cierra el gráfico para poder ser renderizado por graphviz
    print "}"
}
