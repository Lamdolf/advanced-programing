BEGIN{

    #Se crea variable auxiliar para poder imprimir " y se inicia gráfico
    x="\""
    ligand_count = 1 #Contador utilizado en archivo ligandos
    print "digraph G {"
    print "nslimit=1"
    print "nslimit1=1"
    print "maxiter=3"
    print "mclimit=0.1"
    print "searchsize=5"
}

#Primero se procesa archivo de ligandos
FNR==NR{

    #Los centros geométricos ya fueron utilizados y no tienen utilidad,
    #por lo que se ignoran, las interacciones serán procesadas de otra
    #forma mas adelante
    if ($1 != "CENTER" && $1 != "INTER"){

        #Se obtienen algunos datos específicos y se genera un arreglo que
        #simula un diccionario
        ligand[ligand_count]["id"] = $2$3$4
        ligand[ligand_count]["element"] = $1
        ligand[ligand_count]["ligand"] = $2
        ligand[ligand_count]["element_id"] = $1$2$3$4
        ligand_count++
    }

    #Ahora se procesan los detalles de las interacciones
    else if($1 == "INTER"){

        #Se obtienen nuevamente datos en un arreglo que simula diccionario
        id = $2$4
        linked_ligand[id]["print"] = $2 " " $3 " " $4
        linked_ligand[id]["ID"] = $2
        linked_ligand[id]["chain"] = substr($4,4,1)
    }
next
}

#Se procesa archivo de átomos de la proteína
{
    #Datos obtenidos en arreglo tipo diccionario, nuevamente...
    #nótese que ahora en vez de un contador se utiliza FNR, esto debido
    #a que todas las lineas son utilizadas, por lo que FNR (linea actual
    #del archivo, cumple la función de forma mas fácil
    residue[FNR]["element"] = $1
    residue[FNR]["chain"] = $3
    residue[FNR]["id"] = $2$3$4
    residue[FNR]["residue"] = $2
    residue[FNR]["element_id"] = $1$3$4
}

END{

    #Primero se obtienen las cadenas en las que hay un ligando
    for (id in linked_ligand){
        for (chain in linked_ligand[id]){
            if (chain == "chain"){
                chains[id] = linked_ligand[id][chain]
            }
        }
    }

    #Luego se obtienen los ligandos que si interactuan con algo en la
    #distancia entregada por el usuario
    for (id in linked_ligand){
        for (lig in linked_ligand[id]){
            if (lig == "ID"){
                draw[id] = linked_ligand[id][lig]
            }
        }
    }

    #Luego se inicia con la impresión de los átomos y los residuos
    for (i=1; i <= length(residue); i++){
        aux = 1 #Variable auxiliar

        #Se comprueba que se la cadena actual interactua con algún
        #ligando, de lo contrario no se imprime ningun elemento de esta
        for (chain in chains){
            if (chains[chain] == residue[i]["chain"]){
                aux = 0
            }
        }

        #Si la cadena interactúa, esta se imprimirá
        #DISCLAIMER: Los dos segmentos siguientes son identicos al
        #generador anterior, pero contienen cambios en variables
        #o condiciones, por lo que solo uno estará comentado
        if (aux != 1){

            actual=residue[i]["id"]

            #Se revisa si el residuo actual es igual al anterior
            if(actual != anterior){

                #En caso de no serlo, se crea un nuevo modulo con el nombre
                #del residuo
                print ""
                print "node [shape=tripleoctagon]"
                print actual " [label = "x residue[i]["residue"]x"]"

                #Solo si son de la misma cadena, estos se uniran entre si
                if (residue[i]["chain"] == cadena_anterior){
                    print actual " -> " anterior "[label = cadena"residue[i]["chain"]"]"
                }

                print ""
                print "node [shape=invhouse]"
            }

            #Se crea un nuevo nodo por cada elemento, para que no se repitan
            #se toma el nombre del elemento, la cadena y el número de residuo
            print residue[i]["element_id"] " [label = "x residue[i]["element"]x"]"
            print residue[i]["element_id"] " -> " actual "[arrowhead = vee]"

            #Se guardan datos en dos variables al final del bucle, esto ayuda
            #a la conexión y comparación de lineas
            anterior=actual
            cadena_anterior=residue[i]["chain"]

        }
    }


    for (i=1; i <= length(ligand); i++){
        aux = 1

        for (valid in draw){
            if (draw[valid] == ligand[i]["id"]){
                aux = 0
            }
        }

        if (aux != 1){

            actual=ligand[i]["id"]

            if(actual != anterior){

                #Los ligandos tienen otra forma y tienen colores
                #asi es mas fácil indentificarlos
                print ""
                print "node [shape=cylinder, style=filled, fillcolor=pink]"
                print actual " [label = "x ligand[i]["ligand"]x"]"
                print ""
                print "node [shape=Mcircle, style=filled, fillcolor=cyan]"
            }

            print ligand[i]["element_id"] " [label = "x ligand[i]["element"]x"]"
            print ligand[i]["element_id"] " -> " actual "[arrowhead = vee]"

            anterior=actual
        }
    }

    #Se imprimen une cada ligando con el residuo con el que interactúa
    for (id in linked_ligand){
        print linked_ligand[id]["print"]
    }

    #Se termina el archivo .dot cerrando la llave inicial
    print ""
    print "}"

}
