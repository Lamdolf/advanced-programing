BEGIN{
    #Se inicia una variable auxiliar (mas adelante se explica)
    #y se inicia un contador que ayudará a determinar centro geométrico
    aux = 0
    n = 0
}

#Función encargada de procesar datos sobre ligandos presentes en archivo
#pdb que no sean agua (HOH)
{
    #Para evitar cierto error presente en algunas proteínas (ejemplo:
    #1A3N en linea 5262), si la columna de residuo tiene mas de 3 letras,
    #solo se tomarán las 3 últimas
    if (length($4) > 3){
        ligand_aux = substr($4,2,3)
    }
    else{
        ligand_aux = $4
    }

    #Se procesan los datos cuya primera columna sea "HETATM" y no sean agua
    if ($1 == "HETATM" && ligand_aux != "HOH"){

        actual = $5$6

        #se comprueba que se siga contando el mismo ligando, si ya se
        #encuentra en otro, se guardan los resultados del ligando anterior
        #para que no ocurra con la primera linea (dado que no existe un
        #anterior), se utiliza la variable auxiliar
        if(actual != anterior && aux != 0){

            #Se imprime el que el centro del ligando, es la suma de sus
            #coordenadas, divididas en la cantidad de átomos que este tiene
            #(la cantidad dada por el contador, esto sirve igual para iones)
            print "CENTER", ligando, anterior, cor_x/n, cor_y/n, cor_z/n

            #Se reinician los valores de las coordenadas y el contador
            cor_x = 0
            cor_y = 0
            cor_z = 0
            n = 0
        }

        #Siempre se imprimen datos de cada átomo del ligando para ser
        #procesados en la creaccion del gráfico
        print $3, $4, $5, $6

        #Las coordenadas xyz se van acumulando con cada átomo
        cor_x += $7
        cor_y += $8
        cor_z += $9

        #Se guarda ligando anterior y el nombre de este
        ligando = $4
        anterior = actual

        #Despues de la 1 linea, siempre existe un anterior, por lo que
        #la variable auxiliar ahora es 1 y se suma 1 al contador despues de
        #cada salto de linea
        aux = 1
        n++
    }
}

END{
    #Para evitar un error debido a que en la ultima pasada no se compara nada,
    #se imprime el último centro acumulado hasta ahora, asi se evita que el 
    #archivo final carezca de un centro geométrico
    print "CENTER", ligando, anterior, cor_x/n, cor_y/n, cor_z/n
}
