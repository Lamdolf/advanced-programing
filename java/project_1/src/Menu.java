import java.util.Scanner;
import java.util.Random;

public class Menu {

	Scanner sc = new Scanner(System.in);
	int id_nave, cantidad_personas;
	String fuel;
	String tipo_nave;
	
	public void imprimirMenu() {
		
		Random randon = new Random();
		

		int x = 0, a = 0, flag = 0, aux;

		do {
			
			this.id_nave = (randon.nextInt(89)+10);
			System.out.println("\nNUMERO RANDOM PARA ID NAVE: " + id_nave);
			
			System.out.println("MENU PRINCIPAL");
			System.out.println("");
			System.out.println("1. Nave Pesada.");
			System.out.println("2. Nave ligera.");
			System.out.print("TIPO DE NAVE: ");
			aux = sc.nextInt();
			
			switch (aux) {
				
				case 1:
					System.out.println("ESCOGISTE UNA NAVE TIPO PESADA.");
					x = 1;
					this.tipo_nave = "Heavy";
					break;
					
				case 2:
					System.out.println("ESCOGISTE UNA NAVE TIPO LIGERA.");
					x = 1;
					this.tipo_nave = "Light";
					break;
					
				default:
					System.out.println("\nOPCION INGRESADA NO VÁLIDA, INTENTELO NUEVAMENTE.");
					x = 0;
					break;
			}
		
		}while(x == 0);
		
		do {
			System.out.println("\nTIPO DE COMBUSTIBLE: ");
			System.out.println("\n1. Combustible nuclear.");
			System.out.println("2. Combustible Diesel.");
			System.out.println("3. Combustible BioDiesel.");
			System.out.print("COMBUSTIBLE: ");
			aux = sc.nextInt();
			System.out.println("");
			
			switch (aux) {
			
				case 1:
					System.out.println("ESCOGISTE COMBUSTIBLE NUCLEAR.");
					a = 1;
					this.fuel = "Nuclear";
					break;
				
				case 2:
					System.out.println("ESCOGISTE COMBUSTIBLE DIESEL.");
					a = 1;
					this.fuel = "Diesel";
					break;
					
				case 3:
					System.out.println("ESCOGISTE COMBUSTIBLE BIODIESEL.");
					a = 1;
					this.fuel = "Biodiesel";
					break;
					
				default:
					System.out.println("OPCION INGRESADA NO VÁLIDA, INTENTELO NUEVAMENTE.");
					a = 0;
					break;
			}
			
		}while (a == 0);
		
		do {
			
			System.out.print("\n¿CUANTAS PERSONAS PARTICIPARÁN DE LA CARRERA (ADEMÁS DEL JUGADOR) ?: ");
			this.cantidad_personas = sc.nextInt();
			
			if (this.cantidad_personas < 2) {
				System.out.println("\nOPCION INGRESADA NO VÁLIDA.");
				System.out.println("DEBE INGRESAR ALMENOS 2 PERSONAS PARA PODER COMPETIR.");
				flag = 0;
			}
			
			else if (this.cantidad_personas <= 8) {
				System.out.println("\nCANTIDAD DE PERSONAS REGISTRADO CON ÉXITO.");
				flag = 1;
			}
			
			else {
				System.out.println("\nOPCION INGRESADA NO VÁLIDA, INTENTELO NUEVAMENTE.");
				flag = 0;
			}
			
		}while (flag == 0);	
	}
}
