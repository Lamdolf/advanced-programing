
public class Odometer {
	
	private double acceleration;
	private float velocity;
	
	
	Odometer(){
		this.velocity = 0;
	}
	
	public double getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public float getVelocity(String type) {
		update_velocity(type);
		return velocity;
	}
	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}
	
	private void update_velocity(String type) {
		
		
		if (velocity < 300) {
			if (type.equals("Heavy")) {
				this.acceleration = 62.5;
				this.velocity+=(300/288);
			}
		
			else {
				this.acceleration = 75;
				this.velocity+=1.25;
			}
		}
		
		else {
			this.acceleration = 0;
			
		}
	}

}
