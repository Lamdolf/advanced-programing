import java.util.Random;


public class Wheel {

	private int integrity;
	private String status;
	Random rand = new Random();
	
	Wheel(){	
		this.integrity = 100;
		
	}

	public int getIntegrity() {
		return integrity;
	}

	public void setIntegrity(int integrity) {
		this.integrity = integrity;
	}

	public String getStatus(int height) {
		update_state(height); 
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	private void update_state(int height) {
		
		if(height < 50) {
			
			this.status = "Desplegadas";
			this.integrity = this.integrity - (rand.nextInt(11)/10);
			
		}
		
		else {
			this.status = "Plegadas";
		}
	}
	
}
