import java.util.Random;
import java.util.ArrayList;

public class Pista {

	private int longitud = 5000; //5000 m, para 1 min aprox
	
	private ArrayList<Ship> jugadores = new ArrayList();
	Random random = new Random();
	
	private String[] fuel_types = {"Nuclear", "Diesel", "Biodiesel"};
	private String[] ship_types = {"Light", "Heavy"};
	
	
	Pista(int cantidad, int id, String tipo, String fuel){
		
		Ship jugador1 = new Ship(id, tipo, fuel);
		jugadores.add(jugador1);
		
		for(int i=0; i<cantidad; i++) {
			
			Ship bot = new Ship((random.nextInt(89)+10), ship_types[random.nextInt(2)], fuel_types[random.nextInt(3)]);
			
			jugadores.add(bot);
		}
		
	}
	
	public void getShips() {
		
		for (Ship ship : jugadores) {
			
			System.out.println("Nave con ID: " + ship.getId());
			System.out.println("Nave del tipo: " + ship.getBody_type());
			System.out.println("Nave con combustible del tipo: " + ship.getFuel().getType());
			System.out.println("");
			System.out.println("----------------------------------------------------------");
		}
	}
	
	public void getShipsState() {
		
		System.out.println("----------------------------------------------------------------------------------------------------");
		for (Ship ship : jugadores) {
			
			System.out.println("La nave con ID " + ship.getId() + " actualmente viaja a  " + ship.getOdometer().getVelocity(ship.getBody_type()) + "km/h y tiene una aceleracion de: " + ship.getOdometer().getAcceleration());
			System.out.println("----------------------------------------------------------------------------------------------------");
		}
	}
	
	
}
