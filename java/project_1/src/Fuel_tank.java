
public class Fuel_tank {
	
	private int level;
	private String type;
	
	Fuel_tank(String type){
		this.level = 100;
		this.type = type;
	}
	
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
