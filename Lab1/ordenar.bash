#!/bin/bash

if(test -d "$1"); then

    ls -lSr $1>temp
    awk -f counter.awk temp
    rm temp


else
    echo "El directorio seleccionado no existe o no se ha entregado un parámetro"
    echo "Utilice bash ordena.bash /directorio"
    exit 1
fi

