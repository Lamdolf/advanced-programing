#!/bin/bash


if [[ $2 != "" ]]; then

    echo "Solo debe ingresarse un parametro"
    exit 1

elif [[ $1 == '-s' || $1 == '--short' ]]; then

    date | awk '{print $3"/" $2"/" $6}'

elif [[ $1 == '-l' || $1 == '--long' ]]; then

    mes="$(date +%B)"
    day="$(date +%d)"
    year="$(date +%Y)"

    echo "Hoy es $day de $mes del año $year"

elif [[ $1 == "" ]]; then

    cal

else

    echo "Debe ingresar un parametro valido (-s, --short, -l, --long)"
    exit 1
fi
