#!/bin/bash


if (test -d "$HOME/$1"); then
    echo "El archivo seleccionado ya existe"
    echo "ingrese un nuevo nombre de archivo o presione enter para conservar"

    read new_name

    if [[ $new_name != "" ]]; then

        while(test -d $HOME/$new_name); do
            echo "El nombre ya esta en uso, seleccione otro"
            read new_name
        done

        $(mv $HOME/$1 $HOME/$new_name)
        new_dir="$new_name"
    fi
    new_dir="$1"

else
    $(mkdir $HOME/$1)
    new_dir="$1"
    echo "Directorio creado"
fi



for i in $(cat archives.temp); do
    touch $HOME/$new_dir/$i
    echo "created $i"
done


for i in $(ls $HOME/$new_dir | cut -c 1); do

    if (test -d "$HOME/$new_dir/$i"); then
        echo "Existing directory"
    else
        $(mkdir $HOME/$new_dir/$i)
    fi
done


for i in $(cat archives.temp); do

    destination="$(echo $i | cut -c 1)"

    mv $HOME/$new_dir/$i $HOME/$new_dir/$destination
    echo "moved $i to $destination"
done


